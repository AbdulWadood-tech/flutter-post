import 'dart:developer';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mietsofficial/services.dart';

class PostingClass extends StatefulWidget {
  const PostingClass({Key key}) : super(key: key);

  @override
  _PostingClassState createState() => _PostingClassState();
}

class _PostingClassState extends State<PostingClass> {
  var image;
  String description;
  String uid;
  bool check = false;
  bool validation = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 30, horizontal: 20),
            child: Column(
              children: [
                Container(
                  width: double.infinity,
                ),
                Text(
                  'Submit your post',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 30),
                ),
                SizedBox(height: 40),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      width: double.infinity,
                    ),
                    Text(
                      'Select a photo from gallery',
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.blue,
                          fontSize: 11),
                    ),
                    SizedBox(height: 5),
                    InkWell(
                      onTap: () async {
                        image = await Services().selectFile();
                        if (image != null) {
                          setState(() {
                            check = true;
                          });
                        }
                      },
                      child: Container(
                        height: 150,
                        width: 150,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(5),
                            border: Border.all(
                                color: Colors.grey.withOpacity(0.3), width: 1)),
                        child: check
                            ? Image.file(File(image.path))
                            : Icon(
                                Icons.add,
                                size: 50,
                                color: Colors.grey,
                              ),
                      ),
                    ),
                    validation
                        ? Text(
                            'Please pick a picture',
                            style: TextStyle(
                                color: Colors.red, fontWeight: FontWeight.bold),
                          )
                        : SizedBox(),
                  ],
                ),
                SizedBox(height: 40),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Please enter your post description',
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.blue,
                          fontSize: 11),
                    ),
                    Container(
                        height: 150,
                        width: double.infinity,
                        padding: EdgeInsets.symmetric(horizontal: 10),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(5),
                            border: Border.all(
                                color: Colors.grey.withOpacity(0.3), width: 1)),
                        child: TextField(
                          onChanged: (val) {
                            description = val;
                          },
                          decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText: 'Enter your description'),
                        )),
                    validation
                        ? Text(
                            'Please enter post description',
                            style: TextStyle(
                                color: Colors.red, fontWeight: FontWeight.bold),
                          )
                        : SizedBox(),
                  ],
                ),
                SizedBox(height: 50),
                InkWell(
                  onTap: () async {
                    if (image == null || description == null) {
                      setState(() {
                        validation = true;
                      });
                    } else {
                      File file = File(image.path);
                      String url = await Services().addPhotoTStorage(file);
                      print(url);
                      print(Services().getUid());
                      print(DateTime.now().toString());
                      print(description);
                      if (url != null) {
                        print('doing');
                        print('doing');
                        await Services().addPostToRealTimeDatabase(
                            description,
                            url,
                            Services().getUid(),
                            DateTime.now().toString());
                        print('done');
                        print('done');
                      }
                    }
                  },
                  child: Container(
                    padding: EdgeInsets.symmetric(vertical: 10, horizontal: 15),
                    decoration: BoxDecoration(
                        color: Colors.blue,
                        borderRadius: BorderRadius.circular(15)),
                    child: Text(
                      'Submit',
                      style: TextStyle(
                          color: Colors.white, fontWeight: FontWeight.bold),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
