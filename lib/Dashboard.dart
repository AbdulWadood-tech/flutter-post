import 'package:flutter/material.dart';

class DashBoard extends StatefulWidget {
  const DashBoard({Key key}) : super(key: key);

  @override
  _DashBoardState createState() => _DashBoardState();
}

class _DashBoardState extends State<DashBoard> {
  bool check = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.grey.withOpacity(0.2),
        appBar: AppBar(
          title: Text(
            'Home',
            style: TextStyle(color: Colors.blue, fontWeight: FontWeight.bold),
          ),
          backgroundColor: Colors.white,
        ),
        body: SingleChildScrollView(
          child: Column(
            children: [
              SizedBox(height: 20),
              Text(
                'All Posts',
                style: TextStyle(
                    color: Colors.blue,
                    fontWeight: FontWeight.bold,
                    fontSize: 30),
              ),
              ListView(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                children: [
                  PostContainer(
                    imageUrl: 'images/cardriver.jpg',
                    check: check,
                    function: () {
                      setState(() {
                        check = !check;
                      });
                    },
                    onLongPressed: () {
                      print('Loong');
                      return showDialog(
                          context: context,
                          builder: (context) {
                            return Center(
                              child: Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 60),
                                child: Container(
                                  padding: EdgeInsets.symmetric(
                                      vertical: 10, horizontal: 20),
                                  width: double.infinity,
                                  height: 40,
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.circular(15),
                                      boxShadow: [
                                        BoxShadow(
                                          offset: Offset(0, 0),
                                          color: Colors.grey.withOpacity(0.3),
                                          spreadRadius: 1,
                                          blurRadius: 1,
                                        )
                                      ]),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceEvenly,
                                    children: [
                                      Icon(
                                        Icons.thumb_up,
                                        color: Colors.blue,
                                      ),
                                      Icon(
                                        Icons.thumb_up,
                                        color: Colors.blue,
                                      ),
                                      Icon(
                                        Icons.thumb_up,
                                        color: Colors.blue,
                                      ),
                                      Icon(
                                        Icons.thumb_up,
                                        color: Colors.blue,
                                      ),
                                      Icon(
                                        Icons.thumb_up,
                                        color: Colors.blue,
                                      ),
                                      Icon(
                                        Icons.thumb_up,
                                        color: Colors.blue,
                                      ),
                                      Icon(
                                        Icons.thumb_up,
                                        color: Colors.blue,
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            );
                          });
                    },
                  ),
                  PostContainer(
                    imageUrl: 'images/doubtsInMind.jpg',
                    check: check,
                    function: () {
                      setState(() {
                        check = !check;
                      });
                    },
                  ),
                  PostContainer(
                    imageUrl: 'images/experts.jpg',
                    check: check,
                    function: () {
                      setState(() {
                        check = !check;
                      });
                    },
                  ),
                  PostContainer(
                    imageUrl: 'images/programmer.jpg',
                    check: check,
                    function: () {
                      setState(() {
                        check = !check;
                      });
                    },
                  ),
                  PostContainer(
                    imageUrl: 'images/reacher.jpg',
                    check: check,
                    function: () {
                      setState(() {
                        check = !check;
                      });
                    },
                  ),
                ],
              )
            ],
          ),
        ));
  }
}

class PostContainer extends StatelessWidget {
  final String imageUrl;
  final bool check;
  final dynamic function;
  final dynamic onLongPressed;
  PostContainer({this.imageUrl, this.check, this.function, this.onLongPressed});
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
      child: Container(
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(10),
            boxShadow: [
              BoxShadow(
                offset: Offset(0, 0),
                color: Colors.grey.withOpacity(0.1),
                spreadRadius: 1,
                blurRadius: 1,
              )
            ]),
        padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
        child: Column(
          children: [
            Row(
              children: [
                CircleAvatar(
                  backgroundImage: AssetImage('images/testimage.jpg'),
                  radius: 25,
                ),
                SizedBox(width: 10),
                Text(
                  'Abdul Wadood',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
                ),
                Spacer(),
                Icon(
                  Icons.save_alt,
                  color: Colors.grey,
                )
              ],
            ),
            SizedBox(height: 20),
            Text(
              'Hello this is my first post in this, i love this app, also i will upload one of my pictures, this is such an brilliant app, i want to make a lot of friends here',
            ),
            SizedBox(height: 10),
            Container(
              width: double.infinity,
              height: 200,
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage(imageUrl), fit: BoxFit.cover),
                  borderRadius: BorderRadius.circular(5),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.3),
                      offset: Offset(0, 0),
                      spreadRadius: 1,
                      blurRadius: 1,
                    ),
                  ]),
            ),
            SizedBox(height: 20),
            Row(
              children: [
                Icon(
                  Icons.thumb_up,
                  color: Colors.blue,
                  size: 20,
                ),
                Text(
                  '  330',
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                Spacer(),
                Text(
                  '17 Comments',
                  style: TextStyle(fontWeight: FontWeight.bold),
                )
              ],
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 50),
              child: Container(
                width: double.infinity,
                height: 1,
                color: Colors.grey.withOpacity(0.4),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Row(
                children: [
                  InkWell(
                    onTap: function,
                    onLongPress: onLongPressed,
                    child: Icon(
                      check ? Icons.thumb_up : Icons.thumb_up_alt_outlined,
                      size: 20,
                      color: check ? Colors.blue : Colors.grey,
                    ),
                  ),
                  Text(
                    '  Like',
                    style: TextStyle(color: check ? Colors.blue : Colors.grey),
                  ),
                  Spacer(),
                  Icon(
                    Icons.mode_comment_outlined,
                    size: 20,
                    color: Colors.grey,
                  ),
                  Text(
                    '  Comments',
                    style: TextStyle(color: Colors.grey),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
