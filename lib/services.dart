import 'dart:io';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:image_picker/image_picker.dart';
import 'package:firebase_database/firebase_database.dart';

class Services {
  FirebaseAuth firebaseAuth = FirebaseAuth.instance;

  final databaseRef = FirebaseDatabase.instance.reference();

  bool getAuthUser() {
    if (firebaseAuth.currentUser == null) {
      return false;
    } else {
      return true;
    }
  }

  //pick file from gallery
  Future selectFile() async {
    final _picker = ImagePicker();
    var image;
    image = await _picker.pickImage(source: ImageSource.gallery);
    return image;
  }

  //get Current uid
  String getUid() {
    String uid = firebaseAuth.currentUser.uid;
    return uid;
  }

  //add photo to storage
  Future addPhotoTStorage(File file) async {
    final storage = FirebaseStorage.instance;
    var snapshot = await storage.ref().child(file.path).putFile(file);
    String url = await snapshot.ref.getDownloadURL();
    print(url);
    return url;
  }

  //add post to realTime Database
  Future addPostToRealTimeDatabase(String description, String photoUrl,
      String userUid, String dateTime) async {
    return await databaseRef.child('User Posts').push().set({
      'Description': description,
      'Photo URL': photoUrl,
      'DateTime': dateTime,
      'User Uid': userUid,
    });
  }
}
