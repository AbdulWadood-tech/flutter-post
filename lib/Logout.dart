import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class Logout extends StatefulWidget {
  const Logout({Key key}) : super(key: key);

  @override
  _LogoutState createState() => _LogoutState();
}

class _LogoutState extends State<Logout> {

  final FirebaseAuth _auth = FirebaseAuth.instance;
  Logout() async {
    _auth.signOut();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          child: GestureDetector(
            onTap: () {
              Logout();
            },
            child: Container(
              padding: EdgeInsets.all(50),
              color: Colors.blue,
              child: Text("Logout"),
            ),
          ),
        ),
      ),
    );
  }
}
