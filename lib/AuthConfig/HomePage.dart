import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:mietsofficial/Dashboard.dart';
import 'package:mietsofficial/Logout.dart';
import 'package:mietsofficial/posting_screen.dart';

class HomePage extends StatefulWidget {
  ///
//   static PeopleItem userObj;

  const HomePage({Key key}) : super(key: key);
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final fb = FirebaseDatabase.instance;
  FirebaseAuth _auth = FirebaseAuth.instance;

  User user;
  bool isloggedin = false;

  checkAuthentification() async {
    _auth.authStateChanges().listen((user) {
      if (user == null) {
        Navigator.of(context).pushReplacementNamed("SignUp");
      }
    });
  }

  getUser() async {
    user = await _auth.currentUser;
    await user?.reload();
    user = _auth.currentUser;

    if (user != null) {
      setState(() {
        this.user = user;
        this.isloggedin = true;
      });
    }
  }

  @override
  void initState() {
    super.initState();
    // this.checkAuthentification();
    // this.getUser();
  }

  signOut() async {
    _auth.signOut();
  }

  //
  // final PageStorageBucket bucket = PageStorageBucket();
  // Widget currentScreen = DashBoard();
  int currentSelectedIndex = 0;
  getSelectedScreen() {
    if (currentSelectedIndex == 0) {
      return DashBoard();
    } else if (currentSelectedIndex == 1) {
      return PostingClass();
    } else if (currentSelectedIndex == 2) {
      return Logout();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: getSelectedScreen(),
        bottomNavigationBar: BottomNavigationBar(
          onTap: (val) {
            setState(() {
              currentSelectedIndex = val;
            });
          },
          currentIndex: currentSelectedIndex,
          items: [
            BottomNavigationBarItem(icon: Icon(Icons.home), label: 'Home'),
            BottomNavigationBarItem(
                icon: Icon(Icons.post_add_outlined), label: 'Posts'),
            BottomNavigationBarItem(
                icon: Icon(Icons.settings), label: 'Settings')
          ],
        ));
  }
}
