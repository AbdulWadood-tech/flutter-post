import 'dart:convert';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/foundation.dart' show kIsWeb;
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:mietsofficial/AuthConfig/HomePage.dart';
class SignUpPage extends StatefulWidget {
  const SignUpPage({Key key}) : super(key: key);

  @override
  _SignUpPageState createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {

  bool hidePassword = true;
  FirebaseAuth firebaseAuth = FirebaseAuth.instance;
  DatabaseReference reference = FirebaseDatabase.instance.reference().child("App");
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  TextEditingController nameController = new TextEditingController();
  TextEditingController emailController = new TextEditingController();
  TextEditingController passwordController = new TextEditingController();

  SignUpRegisteration() async{
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();
      try {
        String FullName, Email, Password;

        FullName = nameController.text;
        Email = emailController.text;
        Password = passwordController.text;
        UserCredential userCredential = await firebaseAuth
            .createUserWithEmailAndPassword(email: Email, password: Password);
        if (userCredential != null) {
          String AuthID = userCredential.user.uid.toString();
          Map userDataMap = {
            "Name": FullName,
            "Email": Email,
            "ProfileImage": "",
            "Auth": AuthID,
          };

          await reference.child("Users").child(AuthID).set(userDataMap);
          return Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => HomePage()));
        }
      } catch (e) {
        print(e);
      }
    }
  }

  navigateToLogin() async {
    Navigator.pushReplacementNamed(context, "Login");
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      // backgroundColor: Color(0xFFFEFEFE),
      body: SafeArea(
        child: Resmob(),
      ),
    );
  }
  void ShowPassword(){
    setState(() {
      hidePassword = !hidePassword;
    });
  }

  Resmob() {
    return Container(
      child: GestureDetector(
          onTap: (){
            FocusScope.of(context).unfocus();
          },
          child: ListView(
            children: [
              Align(
                alignment: Alignment.topRight,
                child: new IconButton(
                    icon: Icon(
                      Icons.close,color: Colors.grey,),
                    onPressed: () {
                      Navigator.pushReplacementNamed(context, "main");
                    }
                ),
              ),
              SizedBox(height: 60,),
              Center(
                child: Text(
                  'Create Account',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 25,
                  ),
                ),
              ),
              SizedBox(height: 5),
              Center(
                child: Text('Start A Digital Journey',
                  style: TextStyle(
                      fontSize: 11,
                      color: Color(0xFF797979)

                  ),
                ),
              ),
              SizedBox(height: 40,),
              Container(
                child: Form(
                  key: _formKey,
                  child: Column(
                    children: [
                      Center(
                        child: Padding(
                          padding: const EdgeInsets.symmetric(
                              horizontal:20, vertical: 11),
                          child: TextFormField(
                            controller: nameController,
                            validator: (input){
                              if(input.isEmpty) return 'Enter Full-Name';
                              if(input.length<2){ return 'Name must bet at least 2 Charachters'; }
                            },
                            decoration: InputDecoration(
                              hintText: 'Full Name',
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10),
                                borderSide: BorderSide(color: Color(0xFFF5F5F5)),
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Color(0xFFA7A6A6), width: 1
                                ),
                                borderRadius: BorderRadius.circular(10),
                              ),
                              filled: true,
                              fillColor: Colors.white,
                            ),
                            style: TextStyle(
                              fontSize: 15,
                              color: Colors.lightBlueAccent,
                            ),
                          ),
                        ),
                      ),
                      Center(
                        child: Padding(
                          padding: const EdgeInsets.symmetric(
                              horizontal:20, vertical: 9),
                          child: TextFormField(
                            controller: emailController,
                            validator: (input) {
                              if (input.isEmpty) return 'Enter The E-mail';
                              if (!input.contains('@')) {return 'Email Address is invalid';}
                            },
                            decoration: InputDecoration(
                              hintText: 'E-mail',
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10),
                                borderSide: BorderSide(color: Color(0xFFF5F5F5)),
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Color(0xFFA7A6A6), width: 1
                                ),
                                borderRadius: BorderRadius.circular(10),
                              ),
                              filled: true,
                              fillColor: Colors.white,
                            ),
                            style: TextStyle(
                                fontSize: 15,
                                color: Colors.lightBlueAccent
                            ),
                          ),
                        ),
                      ),
                      Center(
                        child: Padding(
                          padding: const EdgeInsets.symmetric(
                              horizontal:20, vertical: 11),
                          child: TextFormField(
                            controller: passwordController,
                            validator: (input) {
                              if (input.isEmpty) return 'Enter The Password';
                              if (input.length < 6) {return 'Provide Minimum 6 Character';}
                            },
                            decoration: InputDecoration(
                              suffixIcon: InkWell(
                                onTap: ShowPassword,
                                child: Icon(
                                  Icons.visibility,
                                  color: Colors.grey,
                                ),
                              ),
                              hintText: 'Password',
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10),
                                borderSide: BorderSide(color: Color(0xFFF5F5F5)),
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Color(0xFFA7A6A6), width: 1
                                ),
                                borderRadius: BorderRadius.circular(10),
                              ),
                              filled: true,
                              fillColor: Colors.white,
                            ),
                            style: TextStyle(
                              fontSize: 15,
                              color: Colors.lightBlueAccent,
                            ),
                            obscureText: hidePassword,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),

              SizedBox(height: 30),
              SizedBox(width: double.infinity,
                child: Container(
                  margin: EdgeInsets.only(left: 20, right: 20),
                  height: 60,
                  child: RaisedButton(
                    onPressed: (){
                      SignUpRegisteration();


                      // if(_name.length < 3)
                      // {
                      //   displayToastMessage("Name must be atleast 2 characters.", context);
                      // }
                      // else if(!_email.contains("@"))
                      // {
                      //   displayToastMessage("Email address is not valid.", context);
                      // }
                      // else if(_password.length < 6)
                      // {
                      //   displayToastMessage("Password must be atleast 5 characters", context);
                      // }

                    },
                    padding: EdgeInsets.all(0.0),
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.0)),
                    child: Ink(
                      decoration: BoxDecoration(
                          gradient: LinearGradient(colors: [
                            Color(0xff2193b0), Color(0xff6dd5ed)],
                            begin: Alignment.centerLeft,
                            end: Alignment.centerRight,
                          ),
                          borderRadius: BorderRadius.circular(8.0)
                      ),
                      child: Container(
                        alignment: Alignment.center,
                        child: Text(
                          "Create Account",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 20,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(height: 15,),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    "Already Have An Account? ",
                    style: TextStyle(
                        fontSize: 14
                    ),
                  ),
                  InkWell(
                    onTap: navigateToLogin,

                    child: Text(
                      " Login",
                      style: TextStyle(
                          fontSize: 16,
                          color: Colors.lightBlueAccent
                      ),
                    ),
                  )
                ],
              ),
              SizedBox(height: 120,),
              Center(
                child: Text('Powered By Lods',
                  style: TextStyle(
                    color: Color(0xFFA7A5A5),
                    fontSize: 12,
                  ),
                ),
              ),
            ],
          )
      ),
    );
  }
  Restab() {
    return Container(
      child: GestureDetector(
          onTap: (){
            FocusScope.of(context).unfocus();
          },
          child: ListView(
            children: [
              Align(
                alignment: Alignment.topRight,
                child: new IconButton(
                    icon: Icon(
                      Icons.close,color: Colors.grey,),
                    onPressed: () {
                      Navigator.pushReplacementNamed(context, "main");
                    }
                ),
              ),
              SizedBox(height: 60,),
              Center(
                child: Text(
                  'Create Account',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 25,
                  ),
                ),
              ),
              SizedBox(height: 5),
              Center(
                child: Text('Start A Digital Journey',
                  style: TextStyle(
                      fontSize: 11,
                      color: Color(0xFF797979)

                  ),
                ),
              ),
              SizedBox(height: 40,),
              Center(
                child: Container(
                  width: 600,
                  child: Column(
                    children: [
                      Center(
                        child: Container(
                          child: Form(
                            key: _formKey,
                            child: Column(
                              children: [
                                Center(
                                  child: Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal:20, vertical: 11),
                                    child: TextFormField(
                                      controller: nameController,
                                      validator: (input){
                                        if(input.isEmpty) return 'Enter Full-Name';
                                        if(input.length<2){ return 'Name must bet at least 2 Charachters'; }
                                      },
                                      decoration: InputDecoration(
                                        hintText: 'Full Name',
                                        enabledBorder: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(10),
                                          borderSide: BorderSide(color: Color(0xFFF5F5F5)),
                                        ),
                                        focusedBorder: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              color: Color(0xFFA7A6A6), width: 1
                                          ),
                                          borderRadius: BorderRadius.circular(10),
                                        ),
                                        filled: true,
                                        fillColor: Colors.white,
                                      ),
                                      style: TextStyle(
                                        fontSize: 15,
                                        color: Colors.lightBlueAccent,
                                      ),
                                    ),
                                  ),
                                ),
                                Center(
                                  child: Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal:20, vertical: 9),
                                    child: TextFormField(
                                      controller: emailController,
                                      validator: (input) {
                                        if (input.isEmpty) return 'Enter The E-mail';
                                        if (!input.contains('@')) {return 'Email Address is invalid';}
                                      },
                                      decoration: InputDecoration(
                                        hintText: 'E-mail',
                                        enabledBorder: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(10),
                                          borderSide: BorderSide(color: Color(0xFFF5F5F5)),
                                        ),
                                        focusedBorder: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              color: Color(0xFFA7A6A6), width: 1
                                          ),
                                          borderRadius: BorderRadius.circular(10),
                                        ),
                                        filled: true,
                                        fillColor: Colors.white,
                                      ),
                                      style: TextStyle(
                                          fontSize: 15,
                                          color: Colors.lightBlueAccent
                                      ),
                                    ),
                                  ),
                                ),
                                Center(
                                  child: Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal:20, vertical: 11),
                                    child: TextFormField(
                                      controller: passwordController,
                                      validator: (input) {
                                        if (input.isEmpty) return 'Enter The Password';
                                        if (input.length < 6) {return 'Provide Minimum 6 Character';}
                                      },
                                      decoration: InputDecoration(
                                        suffixIcon: InkWell(
                                          onTap: ShowPassword,
                                          child: Icon(
                                            Icons.visibility,
                                            color: Colors.grey,
                                          ),
                                        ),
                                        hintText: 'Password',
                                        enabledBorder: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(10),
                                          borderSide: BorderSide(color: Color(0xFFF5F5F5)),
                                        ),
                                        focusedBorder: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              color: Color(0xFFA7A6A6), width: 1
                                          ),
                                          borderRadius: BorderRadius.circular(10),
                                        ),
                                        filled: true,
                                        fillColor: Colors.white,
                                      ),
                                      style: TextStyle(
                                        fontSize: 15,
                                        color: Colors.lightBlueAccent,
                                      ),
                                      obscureText: hidePassword,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),

                      SizedBox(height: 30),
                      SizedBox(width: double.infinity,
                        child: Container(
                          margin: EdgeInsets.only(left: 20, right: 20),
                          height: 60,
                          child: RaisedButton(
                            onPressed: (){
                              SignUpRegisteration();


                              // if(_name.length < 3)
                              // {
                              //   displayToastMessage("Name must be atleast 2 characters.", context);
                              // }
                              // else if(!_email.contains("@"))
                              // {
                              //   displayToastMessage("Email address is not valid.", context);
                              // }
                              // else if(_password.length < 6)
                              // {
                              //   displayToastMessage("Password must be atleast 5 characters", context);
                              // }

                            },
                            padding: EdgeInsets.all(0.0),
                            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.0)),
                            child: Ink(
                              decoration: BoxDecoration(
                                  gradient: LinearGradient(colors: [
                                    Color(0xff2193b0), Color(0xff6dd5ed)],
                                    begin: Alignment.centerLeft,
                                    end: Alignment.centerRight,
                                  ),
                                  borderRadius: BorderRadius.circular(8.0)
                              ),
                              child: Container(
                                alignment: Alignment.center,
                                child: Text(
                                  "Create Account",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 20,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(height: 15,),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            "Already Have An Account? ",
                            style: TextStyle(
                                fontSize: 14
                            ),
                          ),
                          InkWell(
                            onTap: navigateToLogin,

                            child: Text(
                              " Login",
                              style: TextStyle(
                                  fontSize: 16,
                                  color: Colors.lightBlueAccent
                              ),
                            ),
                          )
                        ],
                      ),
                      SizedBox(height: 120,),
                      Center(
                        child: Text('Powered By Lods',
                          style: TextStyle(
                            color: Color(0xFFA7A5A5),
                            fontSize: 12,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          )
      ),
    );
  }
  Resdesk() {
    return Center(
      child: Container(
        width: 1349,
        child: GestureDetector(
            onTap: (){
              FocusScope.of(context).unfocus();
            },
            child: Align(
              alignment: Alignment.centerRight,
              child: Container(
                margin: EdgeInsets.symmetric(vertical: 100,),
                width: 500,
                height: 520,
                decoration: BoxDecoration(
                  color: Colors.white,
                  // boxShadow: [
                  //   BoxShadow(
                  //     color: Color(0xffFAFAFA),
                  //     blurRadius: 30.0, // soften the shadow
                  //     spreadRadius: 1.0, //extend the shadow
                  //   )
                  // ],
                  borderRadius: BorderRadius.circular(10),
                  border: Border.all(
                    color: Color(0xffEAEAEA),
                    width: 0.5,
                  ),
                ),
                child: Column(
                  children: [
                    SizedBox(height: 60,),
                    Center(
                      child: Text(
                        'Create Account',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 25,
                        ),
                      ),
                    ),
                    SizedBox(height: 5),
                    Center(
                      child: Text('Start A Digital Journey',
                        style: TextStyle(
                            fontSize: 11,
                            color: Color(0xFF797979)

                        ),
                      ),
                    ),
                    SizedBox(height: 40,),
                    Container(
                      child: Form(
                        key: _formKey,
                        child: Column(
                          children: [
                            Center(
                              child: Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal:20, vertical: 11),
                                child: TextFormField(
                                  controller: nameController,
                                  validator: (input){
                                    if(input.isEmpty) return 'Enter Full-Name';
                                    if(input.length<2){ return 'Name must bet at least 2 Charachters'; }
                                  },
                                  decoration: InputDecoration(
                                    hintText: 'Full Name',
                                    enabledBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(10),
                                      borderSide: BorderSide(color: Color(0xFFF5F5F5)),
                                    ),
                                    focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Color(0xFFA7A6A6), width: 1
                                      ),
                                      borderRadius: BorderRadius.circular(10),
                                    ),
                                    filled: true,
                                    fillColor: Colors.white,
                                  ),
                                  style: TextStyle(
                                    fontSize: 15,
                                    color: Colors.lightBlueAccent,
                                  ),
                                ),
                              ),
                            ),
                            Center(
                              child: Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal:20, vertical: 9),
                                child: TextFormField(
                                  controller: emailController,
                                  validator: (input) {
                                    if (input.isEmpty) return 'Enter The E-mail';
                                    if (!input.contains('@')) {return 'Email Address is invalid';}
                                  },
                                  decoration: InputDecoration(
                                    hintText: 'E-mail',
                                    enabledBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(10),
                                      borderSide: BorderSide(color: Color(0xFFF5F5F5)),
                                    ),
                                    focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Color(0xFFA7A6A6), width: 1
                                      ),
                                      borderRadius: BorderRadius.circular(10),
                                    ),
                                    filled: true,
                                    fillColor: Colors.white,
                                  ),
                                  style: TextStyle(
                                      fontSize: 15,
                                      color: Colors.lightBlueAccent
                                  ),
                                ),
                              ),
                            ),
                            Center(
                              child: Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal:20, vertical: 11),
                                child: TextFormField(
                                  controller: passwordController,
                                  validator: (input) {
                                    if (input.isEmpty) return 'Enter The Password';
                                    if (input.length < 6) {return 'Provide Minimum 6 Character';}
                                  },
                                  decoration: InputDecoration(
                                    suffixIcon: InkWell(
                                      onTap: ShowPassword,
                                      child: Icon(
                                        Icons.visibility,
                                        color: Colors.grey,
                                      ),
                                    ),
                                    hintText: 'Password',
                                    enabledBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(10),
                                      borderSide: BorderSide(color: Color(0xFFF5F5F5)),
                                    ),
                                    focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Color(0xFFA7A6A6), width: 1
                                      ),
                                      borderRadius: BorderRadius.circular(10),
                                    ),
                                    filled: true,
                                    fillColor: Colors.white,
                                  ),
                                  style: TextStyle(
                                    fontSize: 15,
                                    color: Colors.lightBlueAccent,
                                  ),
                                  obscureText: hidePassword,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),

                    SizedBox(height: 30),
                    SizedBox(width: double.infinity,
                      child: Container(
                        margin: EdgeInsets.only(left: 20, right: 20),
                        height: 60,
                        child: RaisedButton(
                          onPressed: (){
                            SignUpRegisteration();


                            // if(_name.length < 3)
                            // {
                            //   displayToastMessage("Name must be atleast 2 characters.", context);
                            // }
                            // else if(!_email.contains("@"))
                            // {
                            //   displayToastMessage("Email address is not valid.", context);
                            // }
                            // else if(_password.length < 6)
                            // {
                            //   displayToastMessage("Password must be atleast 5 characters", context);
                            // }

                          },
                          padding: EdgeInsets.all(0.0),
                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.0)),
                          child: Ink(
                            decoration: BoxDecoration(
                                gradient: LinearGradient(colors: [
                                  Color(0xff2193b0), Color(0xff6dd5ed)],
                                  begin: Alignment.centerLeft,
                                  end: Alignment.centerRight,
                                ),
                                borderRadius: BorderRadius.circular(8.0)
                            ),
                            child: Container(
                              alignment: Alignment.center,
                              child: Text(
                                "Create Account",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 20,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: 15,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          "Already Have An Account? ",
                          style: TextStyle(
                              fontSize: 14
                          ),
                        ),
                        InkWell(
                          onTap: navigateToLogin,

                          child: Text(
                            " Login",
                            style: TextStyle(
                                fontSize: 16,
                                color: Colors.lightBlueAccent
                            ),
                          ),
                        )
                      ],
                    ),
                  ],
                ),
              ),
            )
        ),
      ),
    );
  }



}
