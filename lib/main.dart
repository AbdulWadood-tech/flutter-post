import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:mietsofficial/AuthConfig/HomePage.dart';
import 'package:mietsofficial/AuthConfig/Login.dart';
import 'package:mietsofficial/AuthConfig/SignUp.dart';
import 'package:mietsofficial/services.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  // await FlutterDownloader.initialize(
  //     debug: true // optional: set false to disable printing logs to console
  // );

  WidgetsFlutterBinding.ensureInitialized();
  await SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
  ]);

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) => MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Services().getAuthUser() ? HomePage() : SignUpPage(),
        routes: {
          "SignUp": (BuildContext context) => SignUpPage(),
          "Login": (BuildContext context) => LoginPage(),
          "HomePage": (BuildContext context) => HomePage(),
        },
      );
}
